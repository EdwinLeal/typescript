import * as _ from 'lodash';
import "./style.scss";
import logo from './img/logo.png';


function component() {
    const element = document.createElement('div');

    const miImagen = new Image();
    miImagen.src = logo;

    element.appendChild(miImagen);


    return element;
}

document.body.appendChild(component());